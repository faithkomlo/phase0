package edu.lehigh.cse216.phase0.android.fko218.phase0_android;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ListView;
import java.util.ArrayList;
import android.widget.AdapterView;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import android.widget.Toast;
import android.util.Log;
import android.support.v7.app.AlertDialog;
import android.widget.TableRow;
import android.widget.AdapterView.OnItemLongClickListener;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        ListView mListView = (ListView) findViewById(R.id.datum_list_view);
        final ArrayList<Datum> myList = new ArrayList<>();
        //myList.add(new Datum("a", "apple"));
        //myList.add(new Datum("b", "banana"));
        //myList.add(new Datum("c", "candybar")); myList.add(new Datum("d", "dinosaur")); String[] listItems = new String[myList.size()]; for (int i = 0; i < myList.size(); i++) {
        //    Datum d = myList.get(i);
        //    listItems[i] = d.val1;
        //}
        //ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems);
        final MyAdapter adapter = new MyAdapter(this, myList);
        mListView.setAdapter(adapter);


        final String data = "[" +
                "{\"val1\":\"a\", \"val2\": \"apple\"}," +
                "{\"val1\":\"b\", \"val2\": \"banana\"}," +
                "{\"val1\":\"c\", \"val2\": \"candy bar\"}," +
                "{\"val1\":\"d\", \"val2\": \"dinosaur\"}," +
                "]";
        try {
            JSONArray arr = new JSONArray(data);
            for (int i = 0; i < arr.length(); ++i) {
                JSONObject o = arr.getJSONObject(i);
                String val1 = o.getString("val1");
                String val2 = o.getString("val2");
                myList.add(new Datum(val1, val2));
            }
        } catch (final JSONException e) {
            Log.e(MainActivity.class.getSimpleName(), "Json parsing error: " + e.getMessage());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            "Json parsing error: " + e.getMessage(),
                            Toast.LENGTH_LONG)
                            .show();
                }
            });
        }


        //Button deleteButton = (Button)mListView.findViewById(R.id.delete_btn);


        registerForContextMenu(mListView);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Datum d = myList.get(position);
                Snackbar.make(view, d.val1 + "\n" + d.val2, Snackbar.LENGTH_LONG)
                        .setAction(d.val2, null).show();

                //Toast.makeText(getApplicationContext(), "HEY you're trying to delete " + myList.indexOf(d), Toast.LENGTH_SHORT).show();
                myList.remove(position);
                adapter.notifyDataSetChanged();
            }
        });


    }

    @Override
    public void onCreateContextMenu(android.view.ContextMenu menu, View view, ContextMenuInfo menuinfo) {
        super.onCreateContextMenu(menu, view, menuinfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
    }

    /**@Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

        int index = info.position;
        Button del = (Button) findViewById(R.id.deleteItem);
        del.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "HEY!", Toast.LENGTH_SHORT).show();
            }
        });

        return true;
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
