package edu.lehigh.cse216.phase0.android.fko218.phase0_android;

import android.widget.BaseAdapter;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.TextView;
import android.widget.Button;


/**
 * Created by faithkomlo on 2/5/17.
 */

public class MyAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    ArrayList<Datum> mData;

    public MyAdapter(Context context, ArrayList<Datum> data) {
        mData = data;
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mData.size();
    }
    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    //public int getPosition(Object item) {
        //return mData.indexOf(item);
    //}

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = mLayoutInflater.inflate(android.R.layout.simple_list_item_1,
                viewGroup, false);
        TextView tv = (TextView) rowView.findViewById(android.R.id.text1);
        tv.setText(mData.get(i).val1);
        return rowView;
    }
}
